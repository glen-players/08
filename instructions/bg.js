/*---------------------------------------------------------
    
    INSTRUCTIONS PAGE INITIATION - MUSIC PLAYER #08
    
---------------------------------------------------------*/

let afterHash;

let urlLastBit = window.location.href.substr(window.location.href.lastIndexOf("/")+1);

if(urlLastBit.indexOf("#") > -1){
    afterHash = urlLastBit.substr(urlLastBit.lastIndexOf("#")+1);
}

document.addEventListener("DOMContentLoaded",() => {
    
    document.querySelectorAll(".tumblr_preview_marker___").forEach(cc => {
        cc.remove();
    })
    
    /*---- LIGHT/DARK MODE OVERLAY ----*/
    let rooty = document.documentElement;
    let ldmOvl = document.querySelector("[ldm-ovl]");
    
    let ldFadeSpeed = getComputedStyle(document.querySelector(":root")).getPropertyValue("--LightDark-Mode-Fade-Speed");
	
	let ldNums = ldFadeSpeed.replace(/[^\d\.]*/g,"");
	let ldSuffix = ldFadeSpeed.replace(ldNums,"");
	
	if(ldSuffix == "s"){
	    ldFadeSpeed = ldNums * 1000;
	} else {
	    ldFadeSpeed = ldNums
	}
	
	if(rooty.matches("[dark-mode]")){
	    let rr = document.querySelector("[light-dark-mode]");
	    rr.querySelector(".sun-mode")
        .style.display = "block";
        
        rr.querySelector(".moon-mode")
        .style.display = "none";
	}
    
    document.querySelectorAll("[light-dark-mode]").forEach(rr => {
        rr.addEventListener("click", () => {
            if(rooty.matches("[dark-mode]")){
                // rooty.removeAttribute("dark-mode")
                
                ldmOvl.style.display = "block";
                
                setTimeout(() => {
                    ldmOvl.classList.add("show-ldm-ovl");
                    
                    setTimeout(() => {
                        document.body.classList.add("hide-scb")
                        rooty.removeAttribute("dark-mode")
                        
                        setTimeout(() => {
                            document.body.classList.remove("hide-scb");
                            
                            ldmOvl.classList.remove("show-ldm-ovl");
                            
                            rr.querySelector(".sun-mode")
                            .style.display = "none";
                            
                            rr.querySelector(".moon-mode")
                            .style.display = "block";
                            
                            setTimeout(() => {
                                ldmOvl.style.display = "none";
                                
                            },ldFadeSpeed)
                            
                        },ldFadeSpeed)
                    },ldFadeSpeed)
                },10)
                
            } else {
                ldmOvl.style.display = "block";
                
                
                setTimeout(() => {
                    ldmOvl.classList.add("show-ldm-ovl");
                    
                    setTimeout(() => {
                        document.body.classList.add("hide-scb")
                        rooty.setAttribute("dark-mode","");
                        
                        setTimeout(() => {
                            document.body.classList.remove("hide-scb");
                            ldmOvl.classList.remove("show-ldm-ovl");
                            
                            rr.querySelector(".moon-mode")
                            .style.display = "none";
                            
                            rr.querySelector(".sun-mode")
                            .style.display = "block";
                            
                            setTimeout(() => {
                                ldmOvl.style.display = "none";
                            },ldFadeSpeed)
                        },ldFadeSpeed)
                    },ldFadeSpeed)
                },10)
            }
            
        })
    })
    
    /*---- LEFT SIDEBAR - OFFSET TOP ----*/
    let leftSB = document.querySelector("[left-sidebar]");
    
    // main content, offset top
    let yqavc = Date.now();
    let bmamo = 2000;
    let rbdql = setInterval(() => {
        if(Date.now() - yqavc > bmamo){
            clearInterval(rbdql)
        } else {
            let leftSB_top = leftSB.offsetTop;
            if(leftSB_top > 0){
                clearInterval(rbdql);
                leftSB.setAttribute("offset-top",leftSB_top);
            }
        }
    })
    
    /*---- GET VW & VH IN PX ----*/
    let gaump = setInterval(() => {
        if(Date.now() - yqavc > bmamo){
            clearInterval(gaump)
        } else {
            let vw = window.innerWidth * 0.01;
            let vh = window.innerHeight * 0.01;
            if(vw > 0 || vh > 0){
                clearInterval(gaump);
                document.querySelector(":root").style.setProperty("--vh",vh + "px");
                document.querySelector(":root").style.setProperty("--vw",vw + "px");
            }
        }
    })
	
	/*---- GRAB CODE SNIPPET FROM GITLAB ----*/
	document.querySelectorAll("[code-block]").forEach(www => {
	    www.style.maxHeight = "calc(var(--vh) * 40)";
	    
	    if(www.matches("[src]")){
	        let fileURL = www.getAttribute("src");
	        
	        if(fileURL !== ""){
	            let exopu = document.createElement("pre");
    	        www.append(exopu);
    	        
    	        fetch(fileURL)
            	.then(data => {
            		return data.text()
            	})
            	.then(data => {
            	   // www.textContent = data
            	   data = data.replaceAll("<","&#60;").replaceAll(">","&#62;")
            	   www.querySelector("pre").innerHTML = data;
            	   hljs.highlightBlock(www);
            	   www.classList.add("show-code-block")
            	})
	        }//end if:fileURL
	        
	    }
	    
	    /*---- RENDER MANUALLY-ENTERED CODE BLOCK ----*/
	    else {
	        let currentCode = www.innerHTML.trim();
	        currentCode = currentCode.replaceAll("<","&#60;").replaceAll(">","&#62;")
	        www.replaceChildren();
	        let whivy = document.createElement("pre");
	        whivy.innerHTML = currentCode;
	        www.append(whivy);
	        
	        if(www.matches("[pre]")){
	            whivy.style.whiteSpace = "pre"
	        }
	        
	        if(www.matches("[pre-line]")){
	            whivy.style.whiteSpace = "pre-line"
	        }
	        
	        hljs.highlightBlock(www);
	        www.classList.add("show-code-block")
	    }
	})
	
	/*---- ARTICLE SECTIONS ----*/
	let sectFadeSpeed = getComputedStyle(document.querySelector(":root")).getPropertyValue("--Sections-Fade-Speed");
	
	let spNums = sectFadeSpeed.replace(/[^\d\.]*/g,"");
	let spSuffix = sectFadeSpeed.replace(spNums,"");
	
	if(spSuffix == "s"){
	    sectFadeSpeed = spNums * 1000;
	} else {
	    sectFadeSpeed = spNums
	}
	
	jqSectFadeSpeed = sectFadeSpeed;
	
	// add FADEOUT to all articles EXCEPT the 1st
	document.querySelectorAll("[article]").forEach((zzz, index) => {
	    if(index > 0){
	        zzz.classList.add("article-fadeout")
	    }
	});
	
	wrapInner({
    	selector: "[nav-link]",
    	innerTag: "button",
    	innerClass: ".ciycw"
    })
	
	document.querySelectorAll("[nav-link]").forEach((uuu, index) => {
	    
	    // add ".active" to first nav item
	    if(index == "0"){
	        uuu.classList.add("active")
	    }
	    
	    uuu.addEventListener("click", () => {
	        // remove ".active" from all other nav items
	        if(!uuu.matches(".active")){
	            uuu.parentNode.querySelectorAll(".active").forEach(aa => {
	                aa.classList.remove("active")
	            })
	            
	            // add ".active" to CURRENT/CLICKED nav item
	            uuu.classList.add("active");
	        }
	        
	        // if [nav-link] has [section-id] attr
    	    if(uuu.matches("[section-id]")){
    	        let navID = uuu.getAttribute("section-id");
    	        
    	        let ogFullURL = window.location;
    	        
    	        let newHashURL = window.location.protocol + "//" + window.location.host + window.location.pathname + "#" + navID;
                window.history.pushState({
                    path: newHashURL
                }, "", newHashURL);
    	        
    	        document.querySelectorAll("[article][section-id]").forEach(ooo => {
    	            // if [article] has [section-id] attr
    	            if(ooo.matches("[section-id]")){
    	                let sectID = ooo.getAttribute("section-id");
    	                
    	                // show selected article
    	                if(sectID == navID){
    	                   setTimeout(() => {
    	                       // ðŸŒ¸ STEP 3
    	                       ooo.style.display = "block";
    	                       
    	                       setTimeout(() => {
    	                           // ðŸŒ¸ STEP 4
    	                           ooo.classList.add("article-fadein")
    	                       },sectFadeSpeed*1)
    	                   },sectFadeSpeed*1)
    	                }
    	                
    	                // hide other articles
    	                else {
    	                   
    	                   // ðŸŒ¸ STEP 1
    	                   ooo.classList.add("article-fadeout");
    	                   ooo.classList.remove("article-fadein");
    	                   
    	                   
    	                   setTimeout(() => {
    	                       // ðŸŒ¸ STEP 2
    	                       ooo.style.display = "none"
    	                   },sectFadeSpeed*1)
    	                }//end: show selected article (hide others)
    	            }//end: if article has [section-id] attr
    	        })//end: [article] forEach
    	    }//end: if [nav-link] has [section-id] attr
    	})//end: [nav-link] click
	})//end: [nav-link] forEach
	
	document.querySelectorAll("a[section-id]").forEach(wz => {
	    wz.setAttribute("tabindex","0");
	    wz.setAttribute("href","javascript:void(0)")
	    
	    wz.addEventListener("click", () => {
	        let sectID = wz.getAttribute("section-id");
	        let matchingNav = document.querySelector("[nav-link][section-id='" + sectID + "']");
	        if(matchingNav !== null){
	            let event = document.createEvent("Event");
                event.initEvent("click", false, true); 
                matchingNav.dispatchEvent(event);
                
                setTimeout(() => {
                    window.scrollTo(0,0)
                },sectFadeSpeed)
	        }
	    })//end inline anchor click
	})//end inline anchor forEach
	
	
	/*---- PAGE LOAD, IF "#' + EXISTS, JUMP THERE ----*/
	let jumpToA = document.querySelector("[nav-link][section-id='" + afterHash + "']");
    if(jumpToA !== null){
        let event = document.createEvent("Event");
        event.initEvent("click", false, true); 
        jumpToA.dispatchEvent(event);
    }
	
	feather.replace();
});//end domloaded
