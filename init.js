/*---------------------------------------------------
    
    MUSIC PLAYER #08 by glenthemes
    
    Initial release: 2023/01/22
    Last updated: 2023/01/22
    
    CREDITS:
    > play icon: Freepik
      flaticon.com/free-icon/play-button_152770
    > pause icon: Pixel Perfect
      flaticon.com/free-icon/pause_608389
    
    it's been a tradition of mine to put hidden
    learning resources here in the comments, so
    let's continue that :)
    
    to build a music player you need javascript
    as well as html components. css is wherever you
    want the player to be and how you want it to look!
    
    > audio javascript:
      https://www.w3schools.com/jsref/dom_obj_audio.asp
    > audio html:
      https://www.w3schools.com/tags/tag_audio.asp
    
---------------------------------------------------*/

document.addEventListener("DOMContentLoaded",() => {

const glenplayer08 = document.querySelector("[glenplayer08]");
const glnPlayIcon = `url("data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>")`;
const glnPauseIcon = `url("data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 320 320' style='enable-background:new 0 0 320 320;' xml:space='preserve'> <g> <g> <path d='M112,0H16C7.168,0,0,7.168,0,16v288c0,8.832,7.168,16,16,16h96c8.832,0,16-7.168,16-16V16C128,7.168,120.832,0,112,0z M96,288H32V32h64V288z'/> </g> </g> <g> <g> <path d='M304,0h-96c-8.832,0-16,7.168-16,16v288c0,8.832,7.168,16,16,16h96c8.832,0,16-7.168,16-16V16C320,7.168,312.832,0,304,0z M288,288h-64V32h64V288z'/> </g> </g> </svg>")`;

let gln08audio, gln08vol, gln08AP, gln08replay;

/*---- GET CSS VARS ----*/
let getRootVar = (bb_bb) => {
	let rootDest = document.querySelector("[glenplayer08]");
	let gkuhe = getComputedStyle(rootDest).getPropertyValue(bb_bb);
	gkuhe = gkuhe.replaceAll("'",'').replaceAll('"',"").trim();
	return gkuhe
}

if(getRootVar("--MusicPlayer-Autoplay") == "yes"){
	gln08AP = "yes";
}

if(getRootVar("--MusicPlayer-Replay") == "yes"){
	gln08replay = "yes";
}

/*---- TARGET [glenplayer08] ----*/
document.querySelectorAll("[glenplayer08]").forEach(lxbrr => {
	
	/*---- MUSIC PLAYER POSITION ----*/
	if(getRootVar("--MusicPlayer-Position").indexOf("custom") > -1){
		lxbrr.setAttribute("position","custom")
	} else {
		if(getRootVar("--MusicPlayer-Position").indexOf("top") > -1){
			lxbrr.setAttribute("position-y","top")
		}
		
		if(getRootVar("--MusicPlayer-Position").indexOf("bottom") > -1){
			lxbrr.setAttribute("position-y","bottom")
		}
		
		if(getRootVar("--MusicPlayer-Position").indexOf("left") > -1){
			lxbrr.setAttribute("position-x","left")
		}
		
		if(getRootVar("--MusicPlayer-Position").indexOf("right") > -1){
			lxbrr.setAttribute("position-x","right")
		}
	}
	
	
	/*---- <audio> STUFF ----*/
	let zovkg = lxbrr.querySelector("audio[src]");
	if(zovkg !== null){
		if(zovkg.getAttribute("src") !== ""){
			gln08audio = lxbrr.querySelector("audio[src]")
			
			if(zovkg.getAttribute("volume") !== ""){
				gln08vol = zovkg.getAttribute("volume").trim();
				if(gln08vol.indexOf("%") > -1){
					gln08audio.volume = Number(gln08vol.replace(/[^\d\.]*/g,"")) / 100;
				}
			}
		}//end: if <audio> has a valid [src]		
	}//end: if <audio> exists
	
	
	/*---- STRAY TEXT NODES (UNWRAPPED) ----*/
	let strayNodes = [];
	let strayText = lxbrr.childNodes;
	for(let i=0; i<strayText.length; i++){
		if(strayText[i].nodeType === 3){
			strayNodes.push(strayText[i]);
		}
	}
	
	strayNodes.forEach(dpmnt => {
		if(dpmnt.textContent.trim() !== ""){
			let okdcg = document.createElement("span");
			dpmnt.replaceWith(okdcg);
			okdcg.appendChild(dpmnt)
		}		
	})
	
	
	/*---- ALBUM IMAGE - WRAP ----*/
	let rcqnu = lxbrr.querySelector("[album-image]");
	let qcufd = document.createElement("div");
	qcufd.setAttribute("album-wrapper","");
	rcqnu.replaceWith(qcufd);
	qcufd.appendChild(rcqnu)
	
	/*---- ALBUM IMAGE - OVERLAY ----*/
	let lnqpi = document.createElement("div");
	lnqpi.setAttribute("mp-ovl","");
	qcufd.prepend(lnqpi);
	
	/*---- ALBUM IMAGE - CONTROLS ----*/
	let jfywv = document.createElement("div");
	jfywv.setAttribute("m-controls","");
	lnqpi.append(jfywv)
	
	/*---- ALBUM IMAGE - PLAY BUTTON ----*/
	let dnkff = document.createElement("div");
	dnkff.classList.add("mp08-play");
	jfywv.append(dnkff);
	
	document.documentElement.style
	.setProperty("--mp08-play-icon",glnPlayIcon);
	
	/*---- ALBUM IMAGE - PAUSE BUTTON ----*/
	let akaob = document.createElement("div");
	akaob.classList.add("mp08-pause");
	jfywv.append(akaob);
	
	document.documentElement.style
	.setProperty("--mp08-pause-icon",glnPauseIcon);
	
	/*---- AUTOPLAY OPTIONS ----*/
	if(gln08AP == "yes"){
		gln08audio.play();
	}
	
	/*---- CORE <audio> STATE EVENTS ----*/
	jfywv.addEventListener("click", () => {
		if(gln08audio.paused){
			gln08audio.play();
			
		} else {
			gln08audio.pause();
		}
	})//end click
	
	gln08audio.addEventListener("play", () => {
		dnkff.style.display = "none";
		akaob.style.display = "block";
	})
	
	gln08audio.addEventListener("pause", () => {
		dnkff.style.display = "block";
		akaob.style.display = "none";
	})
	
	gln08audio.addEventListener("ended", () => {
		if(gln08replay == "yes"){
			dnkff.style.display = "none";
			akaob.style.display = "block";
			gln08audio.currentTime = 0;
			gln08audio.play();
		} else {
			dnkff.style.display = "block";
			akaob.style.display = "none";
		}
		
	})
	
	/*---- WRAP INNER ----*/
	let zrdbx = document.createElement("div");
	zrdbx.setAttribute("m-flex","");
	lxbrr.prepend(zrdbx)
	
	let gmgvx = lxbrr.querySelectorAll(":scope > *:not([m-flex])");
	gmgvx.forEach(ltgft => {
		zrdbx.append(ltgft)
	})
	
	glenplayer08.style.visibility = "visible";
})//end player.each

})//end domcontentloaded
